/// Base class for all [ValueException]
abstract class ValueException<T> implements Exception {
  final T invalidValue;
  final String code;
  final String message;

  const ValueException(this.invalidValue, this.code, this.message);
}

/// Thrown if value is empty
class RequiredValueException extends ValueException {
  const RequiredValueException()
      : super(null, 'required', 'Value is required.');
}

/// Thrown if value does not fit the requirements
class InvalidValueException<T> extends ValueException<T> {
  const InvalidValueException(
    T invalidValue, {
    String code = 'invalid-value',
    String message = 'Invalid value.',
  }) : super(invalidValue, code, message);

  @override
  String toString() {
    return 'Invalid value: $invalidValue';
  }
}

class TooShortValueException<T> extends ValueException<T> {
  const TooShortValueException(T invalidValue)
      : super(invalidValue, 'short', 'Value is too short.');

  @override
  String toString() {
    return 'Value is too short: $invalidValue';
  }
}

class TooLongValueException<T> extends ValueException<T> {
  const TooLongValueException(T invalidValue)
      : super(invalidValue, 'long', 'Value is too long.');

  @override
  String toString() {
    return 'Value is too long: $invalidValue';
  }
}
